<?php

declare(strict_types=1);

namespace Dexodus\UserSessionHistoryBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UserSessionHistoryBundle extends Bundle
{
}
